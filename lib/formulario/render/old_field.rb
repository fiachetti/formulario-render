module Formulario
  module Html
    class Field
      attr_reader :field_name

      def initialize(field_name:, field:)
        @field_name = field_name.to_s
        @field      = field
      end

      private

      attr_reader :field

      def method_missing(method_name, *args, &block)
        field.send(method_name, *args, &block)
      end
    end
  end
end
