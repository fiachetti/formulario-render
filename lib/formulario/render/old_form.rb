module Formulario
  module Html
    class Form
      attr_reader :original_form
      attr_reader :view
      attr_reader :action
      attr_reader :method

      def initialize(original_form, view, action:, method: :post)
        @original_form = original_form
        @view          = view
        @action        = action
        @method        = method
      end

      def fields
        @fields ||= original_form.fields.each_with_object({}) { |(field_name, field), res|
          res[field_name] = Formulario::Html::Field.new(field_name: field_name, field: field)
        }
      end

      def render(&block)
        view.render('forms/_form', locals: {
                      form: self,
                      action: action,
                      method: method,
                    }, &block)
      end

      # Field Types

      def text_field(field_name, label: nil)
        field      = fields[field_name]
        label_text = label || view.titleize(field.field_name)

        view.render "forms/fields/_text_field", locals: {
                      object: original_form,
                      field:  field,
                      field_name:  field_name,
                      value:  field.value,
                      label:  label_text,
                    }
      end

      def text_area(field_name, label: nil)
        field      = fields[field_name]
        label_text = label || view.titleize(field.field_name)

        view.render "forms/fields/_text_area", locals: {
                      object: original_form,
                      field:  field,
                      field_name:  field_name,
                      value:  field.value,
                      label:  label_text,
                    }
      end

      def submit(text='Execute')
        view.render "forms/fields/_submit_button", locals: {
                      text:  text,
                    }
      end

      private

      def method_missing(method_name, *args, &block)
        original_form.send(method_name, *args, &block)
      end
    end
  end
end
